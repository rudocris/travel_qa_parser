class ChangeSourceColumnType < ActiveRecord::Migration
  def change
    add_index :questions, :source
    add_index :answers, :source
    add_index :forums, :source
  end
end

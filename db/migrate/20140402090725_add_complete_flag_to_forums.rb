class AddCompleteFlagToForums < ActiveRecord::Migration
  def change
    add_column :forums, :complete, :boolean
  end
end

class AddCompleteFlagToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :complete, :boolean
  end
end

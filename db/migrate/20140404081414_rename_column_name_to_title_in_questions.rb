class RenameColumnNameToTitleInQuestions < ActiveRecord::Migration
  def change
    rename_column :questions, :name, :title
  end
end

class AddIndexToAnswers < ActiveRecord::Migration
  def change
    add_index :questions, :forum_id
    add_index :forums, :destination_id
  end
end

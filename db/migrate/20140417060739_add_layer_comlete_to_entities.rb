class AddLayerComleteToEntities < ActiveRecord::Migration
  def change
    add_column :forums, :layer_complete, :boolean, default: false
    add_column :questions, :layer_complete, :boolean, default: false
    add_column :answers, :layer_complete, :boolean, default: false
  end
end

class AddDefaultCompleteValue < ActiveRecord::Migration
  def up
    [:destinations, :questions, :answers, :forums].each do |sym|
      change_column sym, :complete, :boolean, default: false
    end
  end

  def down
  end
end

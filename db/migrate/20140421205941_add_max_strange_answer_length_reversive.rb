class AddMaxStrangeAnswerLengthReversive < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up do
        Question.find_each do |question|
          question.update_attribute :max_strange_answer_length, (question.answers.pluck(:length, :author).reject{ |e| e[1] == question.author}.try(:max).try(:[], 0) || 0)
        end
      end
    end
  end
end

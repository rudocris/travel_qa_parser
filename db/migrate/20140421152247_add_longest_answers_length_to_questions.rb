class AddLongestAnswersLengthToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :longest_answer_length, :integer

    reversible do |dir|
      dir.up do
        Question.find_each do |question|
          question.update_attribute :longest_answer_length, question.answers.pluck(:length).try(:max)
        end
      end
    end
  end
end

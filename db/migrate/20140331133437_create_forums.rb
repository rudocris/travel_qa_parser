class CreateForums < ActiveRecord::Migration
  def change
    create_table :forums do |t|
      t.string :url
      t.string :name
      t.integer :destination_id

      t.timestamps
    end
  end
end

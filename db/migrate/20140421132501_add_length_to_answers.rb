class AddLengthToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :length, :integer

    reversible do |dir|
      dir.up do
        Answer.find_each do |answer|
          answer.update_attribute :length, answer.content.length
        end
      end
    end
  end
end

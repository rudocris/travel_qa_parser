class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :url
      t.string :name
      t.integer :forum_id

      t.timestamps
    end
  end
end

class CreateDestinations < ActiveRecord::Migration
  def change
    create_table :destinations do |t|
      t.string :url
      t.string :name

      t.timestamps
    end
  end
end

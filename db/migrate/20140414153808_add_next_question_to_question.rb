class AddNextQuestionToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :next_url, :string
    add_column :questions, :writer, :string
    add_column :answers, :writer, :string
  end
end

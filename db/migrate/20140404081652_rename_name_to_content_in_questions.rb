class RenameNameToContentInQuestions < ActiveRecord::Migration
  def change
    rename_column :answers, :name, :content
  end
end

class AddDefaultValueToLastPageNumber < ActiveRecord::Migration
  def change
    [:forums, :questions, :answers].each do |table|
      change_column table, :last_page_number, :integer, default: 1
    end
  end
end

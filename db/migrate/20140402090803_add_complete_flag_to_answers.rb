class AddCompleteFlagToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :complete, :boolean
  end
end

class AddMarkerToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :marker, :string
  end
end

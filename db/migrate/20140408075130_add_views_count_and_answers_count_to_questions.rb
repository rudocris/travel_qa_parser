class AddViewsCountAndAnswersCountToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :views_count, :integer
    add_column :questions, :answers_count, :integer
  end
end

class RenameWriterToAuthor < ActiveRecord::Migration
  def change
    rename_column :questions, :writer, :author
    rename_column :answers, :writer, :author
  end
end

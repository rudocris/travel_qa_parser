class RenameLastPageUrlToLastPageNumber < ActiveRecord::Migration
  def change
    [:forums, :questions, :answers].each do |table|
      change_column table, :last_page_url, :integer
      rename_column table, :last_page_url, :last_page_number
    end
  end
end

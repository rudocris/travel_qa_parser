class AddSumAnswersLengthToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :total_answers_weight, :integer
  end
end

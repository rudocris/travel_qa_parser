class AddLastPageUrlToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :last_page_url, :string
    add_column :answers, :last_page_url, :string
  end
end

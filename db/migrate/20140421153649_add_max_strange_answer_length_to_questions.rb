class AddMaxStrangeAnswerLengthToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :max_strange_answer_length, :integer
  end
end

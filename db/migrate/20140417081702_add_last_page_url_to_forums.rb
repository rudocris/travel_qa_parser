class AddLastPageUrlToForums < ActiveRecord::Migration
  def change
    add_column :forums, :last_page_url, :string
  end
end

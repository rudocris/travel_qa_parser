class AddDateTimeToEntities < ActiveRecord::Migration
  def change
    [:answers, :questions, :forums].each do |table|
      add_column table, :date, :datetime
    end
  end
end

class AddDefaultValueToQuestion < ActiveRecord::Migration
  def change
    change_column :questions, :marker, :string, default: "unmarked"
  end
end

class AddCompleteFlagToDestination < ActiveRecord::Migration
  def change
    add_column :destinations, :complete, :boolean
  end
end

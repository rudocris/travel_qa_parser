class FixCrashedQuestions < ActiveRecord::Migration
  def change
    Question.where("total_answers_weight IS NOT NULL AND content IS NULL").each do |crashedQuestion|
      Parser::LonleyPlanet.parse_answers_for crashedQuestion
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140620112002) do

  create_table "answers", force: true do |t|
    t.string   "url"
    t.text     "content"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "complete",         default: false
    t.string   "author"
    t.boolean  "layer_complete",   default: false
    t.integer  "last_page_number", default: 1
    t.datetime "date"
    t.integer  "length"
    t.string   "source"
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["source"], name: "index_answers_on_source", using: :btree

  create_table "destinations", force: true do |t|
    t.string   "url"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "complete",   default: false
    t.string   "source"
  end

  create_table "forums", force: true do |t|
    t.string   "url"
    t.string   "name"
    t.integer  "destination_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "complete",         default: false
    t.boolean  "layer_complete",   default: false
    t.integer  "last_page_number", default: 1
    t.datetime "date"
    t.string   "source"
  end

  add_index "forums", ["destination_id"], name: "index_forums_on_destination_id", using: :btree
  add_index "forums", ["source"], name: "index_forums_on_source", using: :btree

  create_table "questions", force: true do |t|
    t.text     "url"
    t.string   "title"
    t.integer  "forum_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "complete",                  default: false
    t.text     "content"
    t.integer  "total_answers_weight"
    t.integer  "views_count"
    t.integer  "answers_count"
    t.string   "marker",                    default: "unmarked"
    t.string   "next_url"
    t.string   "author"
    t.boolean  "layer_complete",            default: false
    t.integer  "last_page_number",          default: 1
    t.datetime "date"
    t.integer  "longest_answer_length"
    t.integer  "max_strange_answer_length"
    t.string   "source"
  end

  add_index "questions", ["forum_id"], name: "index_questions_on_forum_id", using: :btree
  add_index "questions", ["source"], name: "index_questions_on_source", using: :btree

end

require "nokogiri"
require "open-uri"
require 'sidekiq'


class Parser
  class LonleyPlanet
    include Sidekiq::Worker

    sidekiq_options retry: false

    HOME_URL = "https://www.lonelyplanet.com"
    DESTINATIONS_URL = "https://www.lonelyplanet.com/thorntree/categories/country-forums"


    def self.start_parse
      parse_destinations  
    end

    def perform
      self.class.resume_parsing
    end

    def self.resume_parsing
      Destination.where(complete: false, source: nil).each do |destination|
        parse_forums_for destination
        destination.update_attribute :complete, true
      end
    rescue OpenURI::HTTPError => ex
      CrashReporterMailer.crash_email(ex).deliver 

      sleep 60
      resume_parsing
    end

    def self.parse_destinations
      destination_page = Nokogiri::HTML(open(DESTINATIONS_URL))
      destination_page.css(".card--forum__row--content").each do |dst|
        attributes = { 
          url:  "#{HOME_URL}#{dst['data-link-to']}",
          name: dst.at_css("a").text
        }
        destination = Destination.where(attributes).first_or_create
      end
      resume_parsing
    end

    def self.parse_forums_for(destination)
      page = Nokogiri::HTML(open(destination.url))
      page.css("#input_id option").each do |element|
        unless element['value'].empty?
          attributes = {
            url: "#{HOME_URL}#{element['value']}",
            name: element.text
          }
          destination.forums.where(attributes).first_or_create
        end
      end
      if destination.forums.blank?
        destination.forums.create name: "Все страны", url: destination.url
      end
      destination.forums.where(complete: false).each do |forum|
        parse_questions_for forum
        forum.update_attribute :complete, true
      end
    end

    def self.parse_questions_for(forum)
      multipage_parsing entity: forum, selector: ".topic__content" do |params|
        questions = params[:page]
        ([params[:first]] | questions).each do |element|
          author = element.at_css(".user-info__username").text
          qst = element.at_css(".topic__title > a")
          question_url = "#{HOME_URL}#{qst['href']}"
          question_title = qst.text

          answers_count = element.at_css(".posts-count").text[/\d*$/].to_i

          views_count = element.at_css(".views-count").text[/\d*$/]

          views_count.gsub!(/[\.k]/, '') if /\d*\.\dk/.match(views_count)
          attributes = { 
            url: question_url, title: question_title,
            answers_count: answers_count, author: author,
            views_count: views_count.to_i
          }
          forum.update_attribute :last_page_number, params[:page_number]
          question = forum.questions.where(attributes).first_or_create
        end
      end
      forum.questions.where(layer_complete: false).each do |question|
        parse_answers_for question
        question.update_attribute :complete, true if Question.exists? question.attributes
      end
    end

    def self.parse_answers_for(question)
      total_answers_weight = 0

      multipage_parsing entity: question, selector: ".card--forum.replies" do |params|
        question_element = params[:first]

        question_content = question_element.css(".copy--body")[1]
        return question.destroy if question_content.nil?

        question_date = DateTime.parse(question_element.at_css("time")["datetime"])
        question.update_attributes(content: question_content.try(:text),
                                   last_page_number: params[:page_number],
                                   date: question_date)

        answers = params[:page]

        answers.each do |answer|
          author = answer.at_css(".user-info__username")
          if author
            attributes = {
              author: author.text || "",
              content: answer.at_css(".copy--body").text,
              date: DateTime.parse(answer.at_css("time")["datetime"])
            }

            question.answers.where(attributes).first_or_create

            total_answers_weight += attributes[:content].size    
          end
        end
        question.update_attribute :total_answers_weight, total_answers_weight
      end
    end

    def self.multipage_parsing(attrs)
      entity = attrs[:entity]
      url = "#{entity.url}?page=#{entity.last_page_number}"
      page_html = Nokogiri::HTML(open(url))
      page = page_html.css(attrs[:selector])
      first = page.shift
      page << first if page.blank?

      yield({first: first, page: page, page_number: entity.last_page_number})
      pagination_element = page_html.at_css(".pagination__link--last")

      if pagination_element
        pagination_size = pagination_element['href'][/\d*$/].to_i
        start_page = entity.last_page_number + 1

        (start_page..pagination_size).each do |page_number|
          page_url = "#{entity.url}?page=#{page_number}"
          page_html = Nokogiri::HTML(open(page_url))
          page = page_html.css(attrs[:selector])
          yield({first: first, page: page, page_number: page_number})
        end
      end
      entity.update_attribute :layer_complete, true 
    end

  end
end
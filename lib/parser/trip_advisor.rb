require 'nokogiri'
require 'open-uri'
require 'sidekiq'


class Parser
  class TripAdvisor
    include Sidekiq::Worker
  
    sidekiq_options retry: false

    HOME_URL = "http://www.tripadvisor.com"
    DESTINATIONS_URL = "http://www.tripadvisor.com/ForumHome"
    SOURCE = "TripAdvisor"

    def self.start_parse
      parse_destinations
    end

    def perform
      self.class.resume_parsing
    end

    def self.resume_parsing
      Destination.where(complete: false, source: SOURCE).limit(10).map do |destination|
        parse_forums for: destination, from: open(destination.url)
        destination.update_attribute :complete, true
      end
    rescue OpenURI::HTTPError => ex
      CrashReporterMailer.crash_email(ex).deliver 

      sleep 60
      resume_parsing
    end

    def self.parse_destinations
      page_noko = Nokogiri::HTML(open(DESTINATIONS_URL))
      page_html = page_noko.css(".twk_p8")
      destinations_html = page_html[1..-3]
      destinations_html.css("tr a").each do |destination|
        attributes = {
          url: "#{HOME_URL}#{destination['href']}",
          name: destination.text,
          source: SOURCE, 
        }
        Destination.where(attributes).first_or_create
      end
      resume_parsing
    end

    def self.parse_forums(attrs)
      destination = attrs[:for]
      html_page =  attrs[:from]
      page = Nokogiri::HTML(html_page)
      page.css("table.forumtopic tr:nth-child(n+2) .fname a").each do |forum_html|
        attributes = {
          url: "#{HOME_URL}#{forum_html['href']}",
          name: forum_html.text, source: SOURCE
        }
        destination.forums.where(attributes).first_or_create
      end
      if destination.forums.blank?
        destination.forums.create name: "Все страны", url: destination.url
      end
      destination.forums.where(complete: false).each do |forum|
        puts "mltiparsing questions for"
        multiparsing_questions for: forum, from: open(forum.url)
        forum.update_attribute :complete, true
      end
    end

    # def self.resume_parsing_questions_async
    #   Forum.where(complete: false, source: SOURCE).limit(10).each do |forum|
    #     multiparsing_questions for: forum, from: open(forum.url)
    #   end
    #   resume_parsing_answers_async 
    # end

    # def self.resume_parsing_answers_async
    #   Question.where(complete: false, source: SOURCE).limit(3).each do |question|
    #     multiparsing_answers for: question, from: open(question.url)
    #   end
    # end
    
    def self.multiparsing_questions(attrs)
      forum = attrs[:for]      
      html_page = attrs[:from]
      
      page = Nokogiri::HTML(html_page)
      url = forum.url
      prefix = url[/http:\/\/www.tripadvisor.com\/ShowForum-g\d*-i\d*/]
      postfix = url[/\-\w*.html$/]
      page_url = "#{prefix}#{postfix}"

      if pagination_block = page.at_css("#pager_top2")
      
        last_page = pagination_block.css(".paging.taLnk").last
        
        if last_page.present?
          last_page = last_page.text.to_i
          last_question = (last_page - 1) * 20
          
          (20..last_question).step(20).map do |question_number| 
            page_url = "#{prefix}-o#{question_number}#{postfix}"
            
            parse_questions for: forum, from: open(page_url)
          end
        end
      end
      
      parse_questions for: forum, from: open(page_url)
      
      forum.questions.where(layer_complete: false).each do |question|
        puts "multiparsing answers for #{question.title}"
        multiparsing_answers for: question, from: open(question.url)
        question.update_attribute :complete, true if Question.exists? question.attributes
      end
      forum.update_attributes(layer_complete: true) unless last_page.present?
    end

    def self.parse_questions(attrs)
      forum = attrs[:for]
      page_html = attrs[:from]
      page = Nokogiri::HTML(page_html)
      rows = page.css("#SHOW_FORUMS_TABLE tr:nth-child(n+2)")

      rows.each do |row|
        qst = row.css("td:nth-child(3)")
        debugger if qst.nil?
        title, author = qst.text.strip.split("\nby ")
        url = "http://www.tripadvisor.com#{qst.at_css('a')['href']}"
        replies = row.at_css("td:nth-child(4)").text.strip.to_i
        attributes = {
          title: title, author: author, 
          url: url, answers_count: replies,
          source: SOURCE
        }
        forum.questions.where(attributes).first_or_create
      end
    end


    def self.multiparsing_answers(attrs)
      question = attrs[:for]      
      html_page = attrs[:from]
      
      page = Nokogiri::HTML(html_page)
      url = question.url
      prefix = url[/http:\/\/www.tripadvisor.com\/ShowTopic-g\d*-i\d*-k\d*/]
      postfix = url[/\-\w*.html$/]
      page_url = "#{prefix}#{postfix}"

      total_answers_weight = 0
      if pagination_block = page.at_css("#pager_top2")
        last_page = pagination_block.css(".paging.taLnk").last

        if last_page.present?
          last_page = last_page.text.to_i
          last_question = (last_page - 1) * 10
          
          (10..last_question).step(10).map do |question_number| 
            page_url = "#{prefix}-o#{question_number}#{postfix}"
            total_answers_weight += parse_answers for: question, from: open(page_url)
          end
        end
      end
      total_answers_weight += parse_answers for: question, from: open(page_url)
      question.update_attributes(layer_complete: true, total_answers_weight: total_answers_weight)
    end

    def self.parse_answers(attrs)
      question = attrs[:for]
      page_html = attrs[:from]
      page = Nokogiri::HTML(page_html)

      contents = page.css(".post")

      question_content = contents.shift
      question.update_attributes content: question_content.at_css(".postBody").text, 
                                 date: Date.parse(question_content.at_css(".postDate").text)
      total_answers_weight = 0                                 
      contents.each do |post|
        answer = post.at_css(".postBody").text
        
        title, author = answer.strip.split("\nby ")
        if post.at_css(".username")
          attributes = {
            author: post.at_css(".username").text, 
            date: Date.parse(contents.at_css(".postDate").text),
            content: post.at_css(".postBody").text,
            source: SOURCE
          }
          total_answers_weight += attributes[:content].size
          question.answers.where(attributes).first_or_create
        end
      end
      return total_answers_weight
    end

    def self.fix
      Forum.where(source: SOURCE).each do |forum|
        forum.questions.each do |question|
          multiparsing_answers for: question, from: open(question.url)
        end
      end
    end

  end
end
require "savon"
require "eventmachine"
require "nokogiri"

class Parser

  class SMS_T

    def self.hash_session(&block)
      unless defined? @@hashSession 
        login do |hs|
          @@hashSession = hs
        end
      end
      block.call(@@hashSession)
    end

    def self.login(&block)
      EM.synchrony do
        client = Savon.client(wsdl: "http://ws.etsp.ru/Security.svc?singleWsdl")
        
        response = client.call(:logon, message: {"Login" => "stroimash", "Password" => "qwerty249"})

        @@hashSession = response.hash[:envelope][:body][:logon_response][:logon_result]
        puts "LOGIN"
        EM.stop
        block.call(@@hashSession)
      end
      
    end

    def self.price(code)
      EM.synchrony do 
        client = Savon.client(wsdl: "http://ws.etsp.ru/Security.svc?singleWsdl")
        
        response = client.call(:logon, message: {"Login" => "stroimash", "Password" => "qwerty249"})

        hashSession = response.hash[:envelope][:body][:logon_response][:logon_result]

        unless hashSession["error_message"]
            searchClient = Savon.client(wsdl: "http://ws.etsp.ru/Search.svc?singleWsdl")
            response = searchClient.call(:search_basic, message: {'Text' => code.to_s, 'HashSession' => hashSession});
            
            # byebug

            searchResult = response.hash[:envelope][:body][:search_basic_response][:search_basic_result]
            
            unless searchResult['error_message']
              doc = Nokogiri::XML(searchResult)
              
              doc.css("part").each do |part|
                code = part.at_css("code").text.to_s
                puts part.at_css("name").text
                # response = searchClient.call(:part_attendant_by_code, message: {'Text' => code.to_s, 'HashSession' => hashSession});
                priceClient = Savon.client(wsdl: "http://ws.etsp.ru/PartsRemains.svc?singleWsdl")
                
                # puts priceClient.operations
  
                puts part.at_css("is_part_attendant").text

                response = priceClient.call(:get_parts_remains_by_code2, message: {
                  'Code' => code,
                  'ShowRetailRemains' => true,
                  'ShowOutsideRemains' => true,
                  'ShowPriceByQuantity' => true,
                  'HashSession' => hashSession
                })
                
                priceResult = response.hash[:envelope][:body][:get_parts_remains_by_code2_response][:get_parts_remains_by_code2_result]

                doc = Nokogiri::XML(priceResult)

                doc.css("sklad_remains item").map do |part|
                  manuf = part.at_css("manufacturer_name").text
                  price = part.at_css("price").text
                  puts [name, price]
                end

                # index = doc.css("sklad_remains manufacturer_name").map(&:text)
                
                # puts doc.css("sklad_remains price").try(:at, index).try(:text)
              end

            end
          end
          EM.stop
      end
    end

  end
end
class CrashReporterMailer < ActionMailer::Base
  default from: "crash@mailer.copper-line.com"

  def crash_email(error, ctx="EMPTY")
    @error = error
    @ctx = ctx
    @counts = {questions: Question.count, answers: Answer.count}.to_s
    mail to: "rudocris@gmail.com", cc: "mixan946@gmail.com", subject: 'Парсер засрался!'
  end
end

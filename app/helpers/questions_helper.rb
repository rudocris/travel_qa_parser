module QuestionsHelper

  def markers_for(question)    
    Question::MARKERS.each do |marker|
      default_btn_class = if question.marker == marker 
                            "btn btn-default btn-success disabled "
                          else
                            "btn btn-default "  
                          end << marker

      concat link_to(t(marker), 
                     question_change_marker_path(question, marker: marker), 
                     class: default_btn_class, remote: true)

    end
    
    return
  end

end
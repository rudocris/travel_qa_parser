class Destination < ActiveRecord::Base
  has_many :forums
  validates :name, uniqueness: true
end
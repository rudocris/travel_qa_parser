class Forum < ActiveRecord::Base
  belongs_to :destination
  has_many :questions
  validates :url, uniqueness: true
end

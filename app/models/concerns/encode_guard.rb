module EncodeGuard
  extend ActiveSupport::Concern

  included do
    before_save :encode_content, if: :content
  end

  def encode_content
    assign_attributes content: content.gsub(/[\xF0\x9F\x98\x80-\xF0\x9F\x99\x8F]/, '')
  end
end
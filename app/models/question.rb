class Question < ActiveRecord::Base
  include EncodeGuard
  
  MARKERS = ["unmarked","suitable","unsuitable","complete"].freeze

  belongs_to :forum
  has_many :answers
  validates :url, uniqueness: true
  validates :marker, inclusion: { in: MARKERS, allow_nil: true }

  before_save :record_max_strange_answer_length
  before_save :record_longest_answer_length


  def self.dedupe
    grouped = all.group_by{|model| [model.url, model.forum_id] }
    
    grouped.values.each do |duplicates|
      first_one = duplicates.shift
      duplicates.each{|double| double.destroy}
    end
  end
  
  private
  def record_longest_answer_length
    longest_answer_length = answers.pluck(:length).try(:max)
  end

  def record_max_strange_answer_length
    max_strange_answer_length = (answers.pluck(:length, :author).reject{ |e| e[1] == author}.try(:max).try(:[], 0) || 0)
  end
end

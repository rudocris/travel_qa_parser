class QuestionsController < ApplicationController
  http_basic_authenticate_with name: "travel_admin", password: "parsertravelqa"

  def index
    # question_relation = params[:checked] # ? Question.joins(:answers).where("answers.length > ?", params[]) : Question
    if params[:q] = params[:q].presence
      params[:q][:source_null] = true if params[:q][:source_eq] == ""
    end
    
    @search = Question.search(params[:q])
    @questions = @search.result.includes(forum: :destination).uniq.paginate(page: params[:page], per_page: 100)
  end
  
  def show
    @question = Question.find(params[:id])
    @answers = @question.answers.paginate(page: params[:page], per_page: 30)
  end

  def edit
    @question = Question.find(params[:id])
  end

  def change_marker
    @question = Question.find(params[:question_id])
    @question.update_attribute(:marker, params[:marker])
  end

  def parse
    Parser::LonleyPlanet.start_parse_async
    redirect_to root_path
  end
  def parse_ta
    Parser::TripAdvisor.start_parse_async
    redirect_to root_path

  end
  def resume_parse
    Parser::LonleyPlanet.perform_async
    redirect_to root_path
  end
  def resume_parse_ta
    Parser::TripAdvisor.perform_async
    redirect_to root_path
  end
end
